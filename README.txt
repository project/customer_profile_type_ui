Customer Profile Type UI
------------------------

The big idea
------------
The aim of this project is to provide a dynamic way to add new customer profile
types for the Commerce module using a UI. This will mean that anyone wanting
another customer profile type other then the default billing pane that comes
with Commerce can easily add new ones.

This module also adds a new widget (Dynamic profile form manager) for commerce
customer profile reference fields that allows admins to enable/disable customer
profiles on the order edit form using some checkboxes and some slick ajaxiness.

The widget settings include options to wrap the customer profile panes in
collapsible fieldsets. This becomes especially useful if you have many customer
profile types that clutter the order edit form. The same settings are also added
to the default customer profile manager widget that ships with commerce.


Enable the module
-----------------
!!! After enabling the module you will most likely want to clear all caches so
that the new menu items, etc appear.


Creating new Customer Profiles
------------------------------

  1. Click the new 'Add customer profile type' button on the profile types page. 
     It will also be available in the admin menu:
       store > profiles > add customer profile type
  2. Fill out the form and click save.
  3. Click manage fields or display to add fields to your new customer profile
     type and control how it is displayed.
  4. Finally go to the order configuration page and drag/drop the new profiles
     into the order checkout regions you want them to appear in.
  5. Dynamically disable profile panes using rules


Rules modle integration
-----------------------
  This module also adds a new rule event and actions that will allow for the
  dynamic disabling of customer profile panes during the checkout process.

  New rule event: 'Before customer profile panes are displayed'
  New rule actions:

   'Disable all customer profile panes' - disables all customer profile panes
                                          on the order.
   'Disable a customer profile pane' - allows you to select a specific customer
                                       profile type to disable.
   'Enable a customer profile pane' - allows you to select a specific customer
                                      profile type to enable.

  By default all customer profile panes that have been placed in checkout regions
  will be enabled and appear in the checkout process.

How to enable/disable customer profile panes
--------------------------------------------

  During checkout
  ---------------

  By default all customer profile panes that have been placed in checkout
  regions will be enabled and appear in the checkout process.

    1. Create a new rule that is triggered on the 'Before customer profile panes
       are displayed' event.
    2. Add in some conditions. One particular use case is for a membership
       system where different membership types require different customer profile
       fields. So my conditions involve the order's products.
    3. Add as many 'Disable/Enable a customer profile pane' actions as panes you
       wish to disable. By default all panes are enabled. I tend to use the
       'disable all' action and then follow it by enabling actions for the panes
       I want to display.

  On the order edit page
  ----------------------

  By default all customer profile forms will be displayed on the order edit page
  regardless whether they were enabled during checkout or if a profile of that
  type has even been created for the order yet. (Commerce assumes you want to
  fill out these forms, however this is not always the case.)

  1. For customer profile panes that you do not want displayed in every order,
     change the widget for that profile reference field on the order's manage
     fields page (admin/commerce/config/order/fields).
  2. Select the 'dynamic profile form manager' widget from the options.
  3. To toggle the collapsed/collapsible state of each customer profile form on
     the order edit page, click edit on the respective field. Change the 'Field
     form wrapper' setting to your preference. This setting also appears on the
     default 'customer profile manager' widget so that you can set fields using
     that widget to also be collapsible.


Credits:
--------
Created, designed and developed by Jesse Payne (acrazyanimal)


